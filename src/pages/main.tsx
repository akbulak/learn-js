import React, {useState, useCallback} from 'react';

function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

const Main = () => {
    const [value, setValue] = useState('')
    const handleChange = useCallback((event) => {
        sleep(111)
    setValue(event.target.value) 
    }, [])
    return(
        <div><h2>Главная cтраница</h2><input onChange={handleChange} value={value} type="text" /> </div>
    )
}

export default Main;