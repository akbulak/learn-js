import React from 'react';
import Header from '../header';
const PageWrapper = (props) => {
    return(
        <div>
            <Header/>
            {props.children}
        </div>
    )
}

export default PageWrapper;