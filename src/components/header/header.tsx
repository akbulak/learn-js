import React from "react";

import { Wrapper, NavBlock, PhoneBlock } from "./header.style";
import {CustomLink } from './custom-link'
import { Logo, PhoneIcon } from "../../assets/svg-icons";

const Header = () => {
  return (
    <Wrapper>
      <Logo />
      <NavBlock>
        <CustomLink to="/">главная</CustomLink>
        <CustomLink to="/sections">секции</CustomLink>
        <CustomLink to="/news">новости</CustomLink>
      </NavBlock>
      <PhoneBlock> <PhoneIcon/> 8 (495) 640-91-57</PhoneBlock>
    </Wrapper>
  );
};

export default Header;
