import React from "react";
import { useResolvedPath, useMatch } from "react-router-dom";

import { NavLink } from "./custom-link.style";

export const CustomLink = ({ children, to, ...props }) => {
  let resolved = useResolvedPath(to);
  let match = useMatch({ path: resolved.pathname, end: true });

  return (
    <NavLink to={to} {...props} match={match}>
      {children}
    </NavLink>
  );
};
