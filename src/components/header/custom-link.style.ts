import styled from '@emotion/styled'
import {Link } from "react-router-dom";

export const NavLink:any = styled(Link)`
margin-right: 10px;
color: #003EFF;
font-weight: bold;
font-size: 14px;
text-decoration: none;

${(props:any) => props.match && 'border-bottom: #003EFF 3px solid;' }
`