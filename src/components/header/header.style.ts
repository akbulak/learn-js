import styled from '@emotion/styled'


export const Wrapper = styled.section`
display: grid;
grid-template-columns:268px minmax(300px, 779px) 393px;
align-items: center;
padding: 20px;
`;

export const NavBlock = styled.div`
    display: flex;
    align-items: stretch;
    justify-content: center;
    align-content: flex-end;
`
export const PhoneBlock = styled.div`
font-weight: 600;
font-size: 16px;
color: #1E1D20;
display: flex;
align-items: center;
> svg {
    margin-right: 8px;
}
`

