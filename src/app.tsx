import React from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";

import { Wrapper } from "./app.style";
import PageWrapper from "./components/page-wrapper";
import News from "./pages/news";
import Main from "./pages/main";
import Sections from "./pages/sections";

const App = () => {
  return (
    <Wrapper>
      <BrowserRouter basename="/learn">
        <PageWrapper>
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="sections" element={<Sections />} />
            <Route path="news" element={<News />} />
          </Routes>
        </PageWrapper>
      </BrowserRouter>
    </Wrapper>
  );
};

export default App;
